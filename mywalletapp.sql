-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2018 at 12:40 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mywalletapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `consumable_expenses`
--

CREATE TABLE `consumable_expenses` (
  `consumable_id` smallint(5) UNSIGNED NOT NULL,
  `consumable_name` varchar(50) NOT NULL,
  `consumable_amount` double UNSIGNED NOT NULL,
  `month_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consumable_expenses`
--

INSERT INTO `consumable_expenses` (`consumable_id`, `consumable_name`, `consumable_amount`, `month_id`) VALUES
(1, 'food', 3500, 1),
(2, 'personal hygiene', 1000, 1),
(3, 'fare', 400, 1),
(4, 'food', 3500, 2),
(5, 'personal hygiene', 800, 2),
(6, 'fare', 400, 2),
(7, 'food', 3000, 3),
(8, 'personal hygiene', 900, 3),
(9, 'fare', 400, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fix_expenses`
--

CREATE TABLE `fix_expenses` (
  `fix_id` smallint(5) UNSIGNED NOT NULL,
  `fix_expense_name` varchar(50) NOT NULL,
  `fix_value` double UNSIGNED NOT NULL,
  `month_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fix_expenses`
--

INSERT INTO `fix_expenses` (`fix_id`, `fix_expense_name`, `fix_value`, `month_id`) VALUES
(1, 'rent', 3000, 1),
(2, 'lending', 400, 1),
(3, 'counterpart', 500, 1),
(4, 'rent', 3000, 2),
(5, 'lending', 400, 2),
(6, 'counterpart', 500, 2),
(7, 'rent', 3000, 3),
(8, 'lending', 400, 3),
(9, 'counterpart', 500, 3);

-- --------------------------------------------------------

--
-- Table structure for table `month`
--

CREATE TABLE `month` (
  `month_id` smallint(5) UNSIGNED NOT NULL,
  `month_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `month`
--

INSERT INTO `month` (`month_id`, `month_name`) VALUES
(1, 'January'),
(2, 'February'),
(3, 'March'),
(4, 'April'),
(5, 'May'),
(6, 'June'),
(7, 'July'),
(8, 'August'),
(9, 'September'),
(10, 'October'),
(11, 'November'),
(12, 'December');

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `sal_id` smallint(5) UNSIGNED NOT NULL,
  `amount` double UNSIGNED NOT NULL,
  `month_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`sal_id`, `amount`, `month_id`) VALUES
(1, 0, 1),
(2, 0, 2),
(3, 0, 3),
(4, 0, 4),
(5, 0, 5),
(6, 0, 6),
(7, 0, 7),
(8, 0, 8),
(9, 0, 9),
(10, 0, 10),
(11, 0, 11),
(12, 0, 12);

-- --------------------------------------------------------

--
-- Table structure for table `savings`
--

CREATE TABLE `savings` (
  `save_id` smallint(5) UNSIGNED NOT NULL,
  `percentage` smallint(5) UNSIGNED NOT NULL,
  `month_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `savings`
--

INSERT INTO `savings` (`save_id`, `percentage`, `month_id`) VALUES
(1, 0, 1),
(2, 0, 2),
(3, 0, 3),
(4, 0, 4),
(5, 0, 5),
(6, 0, 6),
(7, 0, 7),
(8, 0, 8),
(9, 0, 9),
(10, 0, 10),
(11, 0, 11),
(12, 0, 12);

-- --------------------------------------------------------

--
-- Table structure for table `scheduled_expenses`
--

CREATE TABLE `scheduled_expenses` (
  `sched_id` smallint(5) UNSIGNED NOT NULL,
  `sched_expense_name` varchar(50) NOT NULL,
  `sched_amount` double UNSIGNED NOT NULL,
  `month_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheduled_expenses`
--

INSERT INTO `scheduled_expenses` (`sched_id`, `sched_expense_name`, `sched_amount`, `month_id`) VALUES
(1, 'water bill', 700, 1),
(2, 'electricity', 500, 1),
(3, 'debt', 600, 1),
(4, 'water bill', 778.12, 2),
(5, 'electricity', 621.23, 2),
(6, 'debt', 600, 2),
(7, 'water bill', 852.67, 3),
(8, 'electricity', 589.43, 3),
(9, 'debt', 600, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `consumable_expenses`
--
ALTER TABLE `consumable_expenses`
  ADD PRIMARY KEY (`consumable_id`),
  ADD KEY `month_id` (`month_id`);

--
-- Indexes for table `fix_expenses`
--
ALTER TABLE `fix_expenses`
  ADD PRIMARY KEY (`fix_id`),
  ADD KEY `month_id` (`month_id`);

--
-- Indexes for table `month`
--
ALTER TABLE `month`
  ADD PRIMARY KEY (`month_id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`sal_id`),
  ADD KEY `month_id` (`month_id`);

--
-- Indexes for table `savings`
--
ALTER TABLE `savings`
  ADD PRIMARY KEY (`save_id`),
  ADD KEY `month_id` (`month_id`);

--
-- Indexes for table `scheduled_expenses`
--
ALTER TABLE `scheduled_expenses`
  ADD PRIMARY KEY (`sched_id`),
  ADD KEY `month_id` (`month_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `consumable_expenses`
--
ALTER TABLE `consumable_expenses`
  MODIFY `consumable_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `fix_expenses`
--
ALTER TABLE `fix_expenses`
  MODIFY `fix_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `month`
--
ALTER TABLE `month`
  MODIFY `month_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `sal_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `savings`
--
ALTER TABLE `savings`
  MODIFY `save_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `scheduled_expenses`
--
ALTER TABLE `scheduled_expenses`
  MODIFY `sched_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `consumable_expenses`
--
ALTER TABLE `consumable_expenses`
  ADD CONSTRAINT `consumable_expenses_ibfk_1` FOREIGN KEY (`month_id`) REFERENCES `month` (`month_id`);

--
-- Constraints for table `fix_expenses`
--
ALTER TABLE `fix_expenses`
  ADD CONSTRAINT `fix_expenses_ibfk_1` FOREIGN KEY (`month_id`) REFERENCES `month` (`month_id`);

--
-- Constraints for table `salary`
--
ALTER TABLE `salary`
  ADD CONSTRAINT `salary_ibfk_1` FOREIGN KEY (`month_id`) REFERENCES `month` (`month_id`);

--
-- Constraints for table `savings`
--
ALTER TABLE `savings`
  ADD CONSTRAINT `savings_ibfk_1` FOREIGN KEY (`month_id`) REFERENCES `month` (`month_id`);

--
-- Constraints for table `scheduled_expenses`
--
ALTER TABLE `scheduled_expenses`
  ADD CONSTRAINT `scheduled_expenses_ibfk_1` FOREIGN KEY (`month_id`) REFERENCES `month` (`month_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
