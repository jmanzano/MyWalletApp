/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mywalletapp;

/**
 *
 * @author student
 */
public abstract class Expenses {
    private String expense_name;
    private double expense_amount;

    public Expenses() {
    }

    public Expenses(String expense_name, double expense_amount) {
        this.expense_name = expense_name;
        this.expense_amount = expense_amount;
    }

    public String getExpense_name() {
        return expense_name;
    }

    public void setExpense_name(String expense_name) {
        this.expense_name = expense_name;
    }

    public double getExpense_amount() {
        return expense_amount;
    }

    public void setExpense_amount(double expense_amount) {
        this.expense_amount = expense_amount;
    }
   
}
