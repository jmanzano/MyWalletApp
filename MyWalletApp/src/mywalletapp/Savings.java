/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mywalletapp;

/**
 *
 * @author student
 */
public class Savings {
    private int percentageFromSalary;
    private Salary amountSalary;

    public Savings() {
    }

    public Savings(int percentageFromSalary) {
        this.percentageFromSalary = percentageFromSalary;
    }
    
    public Savings(int percentageFromSalary, Salary amountSalary) {
        this.percentageFromSalary = percentageFromSalary;
        this.amountSalary = amountSalary;
    }

    public int getPercentageFromSalary() {
        return percentageFromSalary;
    }

    public void setPercentageFromSalary(int percentageFromSalary) {
        this.percentageFromSalary = percentageFromSalary;
    }

    public Salary getAmountSalary() {
        return amountSalary;
    }

    public void setAmountSalary(Salary amountSalary) {
        this.amountSalary = amountSalary;
    }
    
    public double getRemainingMoney() {
        return amountSalary.getAmount() - (amountSalary.getAmount() * ((double)percentageFromSalary / 100));
    }
    
    public double getSavingsMoney() {
        return amountSalary.getAmount() * ((double)percentageFromSalary / 100);
    }
}
